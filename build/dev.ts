import { getWebpackConfig, getUtils } from 'radic-dev'
import { Loader, Configuration, Plugin, Rule } from "webpack";
import * as webpack from 'webpack';
import { resolve } from "path";
import configs from './configs'
import * as Tapable from "tapable";
import * as _ from "lodash";


const config = configs.dev;
const utils  = getUtils(config);
const wp     = getWebpackConfig(config, utils);
// let cssLoaders              = utils.cssLoaders({
//     sourceMap: config.productionSourceMap,
//     extract  : config.extractCss
// })
// cssLoaders[ 'scss' ]        = cssLoaders[ 'scss' ].replace('sass-loader', 'resolve-url-loader!sass-loader')
const wpconf: Configuration = {
    ...wp.dev,
    entry  : {
        radic: utils.srcPath('index.ts')
    },
    output : {
        path         : config.assetsRoot,
        publicPath   : config.assetsPublicPath,
        filename     : utils.assetsPath('js/[name].js'),
        chunkFilename: utils.assetsPath('js/[id]-[hash].js'),
        library      : 'radic',
        libraryTarget: 'umd'
    },
    resolve: {
        alias     : {

            'jquery': 'jquery/src/jquery'
        },
        extensions: [ '.js', '.vue', '.css', '.json', '.ts', '.jsx', '.tsx' ]
    },
    node   : <any> {
        process: false,
        fs     : "empty",
        path   : true,
        url    : false
    },
    module: {
        rules: [
            wp.loader('vue'),
            wp.loader('ts', {
                exclude: [ '!node_modules/metronic', 'node_modules' ],
            }),
            wp.loader('json'),
            wp.loader('pug'),
            wp.loader('md'),
            wp.loader('es6', {
                exclude: [ '!node_modules/metronic', 'node_modules' ],
            }),
            wp.loader('font'),
            wp.loader('image')
        ].concat(utils.styleLoaders({
            sourceMap: config.productionSourceMap,
            extract: config.extractCss
        }))
    },
    plugins: <Tapable.Plugin[]> [
        wp.plugins.env(),
        wp.plugins.hot(),
        wp.plugins.namedModuleIds(),
        wp.plugins.clean([ config.assetsRoot ], { root: process.cwd() }),
        wp.plugins.html(utils.srcPath('index.html'), 'index.html', { inject: true }),
        wp.plugins.favico(utils.assetsPath('metronic/layouts/layout6/img/logo.png'), 'assets/img/favicons'),
        wp.plugins.order(),
        wp.plugins.vendorChunks(),
        wp.plugins.manifestChunks(),
        wp.plugins.extractCss(utils.assetsPath('css/[name].css')),
        new webpack.ProvidePlugin({
            jQuery: 'jquery',
            $     : 'jquery',
            'window.jQuery': 'jquery'
        })
    ]
}

export = wpconf

if ( ! module.parent ) {
    console.dir(wpconf, { colors: true, depth: 10 })
}
