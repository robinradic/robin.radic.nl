
interface JQueryStatic<TElement extends Node = HTMLElement>{
    notific8:any
    blockUI:any
    unblockUI()
    fancybox:any
}

interface JQuery<TElement extends Node = HTMLElement> extends Iterable<TElement> {
    iCheck(opts?:any):this
    bootstrapSwitch(opts?:any):this
    confirmation(opts?:any):this
    tabdrop(opts?:any):this
    block(opts?:any):this
    unblock(opts?:any):this
    slimScroll(opts?:any):this
    fancybox(opts?:any):this
    counterUp(opts?:any):this
select2:Select2Plugin
}

