import './styles/stylesheet.scss'
import './styles/custom.scss'
import 'js-cookie'
import 'jquery-slimscroll'
import 'bootstrap'
import './vendor/jquery.blockui.js'

import App from './vendor/app';
import Layout from './vendor/layout'
import { merge } from 'lodash';



export async function init(options:any={}){
    options = merge({
        quickSidebar: false,
        quickNav: false
    }, options)

    App.init()
    Layout.init()

    if(options.quickSidebar){
        const {QuickSidebar} = await import('./vendor/quick-sidebar');
        console.log({QuickSidebar})
        QuickSidebar.init()
    }
    if(options.quickNav){
        const QuickNav:any = await import('./vendor/quick-nav');
        QuickNav.init()
    }

    if(options.testAsync){
        const testasync = await import('./styles/test-async.scss')
        console.log({testasync })
    }

    return Promise.resolve()
}
