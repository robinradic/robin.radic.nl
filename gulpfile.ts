import * as gulp from 'gulp';
import * as gulpTs from 'gulp-typescript';
import * as clean from 'gulp-clean';
import * as copy from 'gulp-copy';
import * as sequence from 'run-sequence';
import * as pump from 'pump';
import * as googleWebFonts from 'gulp-google-webfonts';

import * as webpack from 'webpack';
import { rollup } from 'rollup';
import * as rollupTs from 'rollup-plugin-typescript';
import { join } from "path";
import { Golp } from 'radic-dev';
import configs from './build/configs'

let config = configs.dev
const golp = new Golp(gulp);

gulp.task('set-dev', () => config = configs.dev);
gulp.task('set-dist', () => config = configs.prod);

golp.add
    .taskList('tasks', true)
    .webpack({
        prefix    : 'dev',
        configFile: 'build/dev.ts'
    })
    .webpack({
        prefix    : 'dist',
        configFile: 'build/dist.ts'
    })



gulp.task('clean:metronic', () => pump(gulp.src('assets/metronic'), clean()))
gulp.task('clean:fonts', () => pump(gulp.src('assets/fonts'), clean()))



gulp.task('init:metronic', [ 'clean:metronic' ], () => pump(gulp.src([ 'node_modules/metronic/assets/**', '!**/*.js' ]), copy('assets/metronic/', { prefix: 3 })))

gulp.task('init:fonts:icons', () => pump(gulp.src([ 'node_modules/font-awesome/fonts/**', 'node_modules/simple-line-icons/fonts/**' ]), copy('assets/fonts/', { prefix: 3 })))
gulp.task('init:fonts:google', () => pump(gulp.src('src/fonts.list'), googleWebFonts({}), gulp.dest('assets/fonts')))
gulp.task('init:fonts', [ 'clean:fonts' ], () => sequence([ 'init:fonts:icons', 'init:fonts:google' ]))

gulp.task('init', () => sequence([ 'init:metronic', 'init:fonts' ]))



